import pgzrun

TITLE = 'Flappy Bird'
WIDTH = 400
HEIGHT = 708

# These constants control the difficulty of the game
GRAVITY = 0.3
FLAP_STRENGTH = 6.5

bird = Actor('bird1', (75, 200))
bird.dead = False
bird.score = 0
bird.vy = 0

def update_bird():
    uy = bird.vy
    bird.vy += GRAVITY
    bird.y += (uy + bird.vy) / 2
    bird.x = 75

    if not bird.dead:
        if bird.vy < -3:
            bird.image = 'bird2'
        else:
            bird.image = 'bird1'

    if not 0 < bird.y < 720:
        bird.y = 200
        bird.dead = False
        bird.score = 0
        bird.vy = 0

def update():
    update_bird()

def on_key_down():
    if not bird.dead:
        bird.vy = -FLAP_STRENGTH

def draw():
    screen.blit('background', (0, 0))
    bird.draw()




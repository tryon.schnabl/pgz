Intro to game development with pygame zero 
------------------------------------------

pygame is an awesome 2d-game framework written in python. It is quite powerful and allows you to write quite complex games in it. The learning curve is a little steep, so we're going to use a framework called "pygame zero". It is based on pygame, but abstracts away a bunch of things that you don't need to worry about just yet. 

We'll be working through two examples from the pygame zero tutorials, then we'll start to write our own games.

What is a game?
---------------
Let's consider the first game called "alien". In it, we'll have a little alien character moving across a screen. When you click on it with your mouse, he'll react!


SO, there are already a few elements going on here. First, we'll need a canvas to work on. 

+----------+
|          |
|          |
+----------+

Next, we'll need a sprite or a character (pygame calls these "actors").

Something will happen (the alien will move). So, time is going on. 

And finally, events will happen (mouse clicks), that we'll have to worry about. 

We'll go through these steps one by one. 

pygame zero
------------
Firstly, pygame zero expects our project to be layed out as follows: 
.
├── images/
│   └── alien.png
│   └── alien_hurt.png
├── sounds/
│   └── eep.wav
└── alien.py

where alien.py is where your game code is. It will look for images for sprites in "images" and sounds in "sounds". 

It will also just refer to the sprites by their name only (you don't need the .png)

game loop
---------
in pygame and pygame zero, there is a main event loop happening. Each "tick" or time interval it does the following

1. checks for "events", these are things like keyboard and mouse actions
2. runs whatever is in "update()"
3. runs whatever is in "draw()"

If you think about frames in an animation or movie, it looks like this: 

draw()           draw()            draw()
+----+           +----+            +----+
|    |  update() |    |  update()  |    |
|    |           |    |            |    |
+----+           +----+            +----+

update() will end up getting called about 60 times a second!

Other 
-----
To quit - Ctrl-Q
origin (0,0) is in the top left corner


#alien
https://pygame-zero.readthedocs.io/en/stable/introduction.html

#flappy bird
https://github.com/lordmauve/pgzero/examples/flappybird

#ideas
0. Change the numbers/sprites/backgrounds/sounds. See how it changes the game. 
1a. Change alien so that the canvas is larger, and the alien can move in any direction, not just left to right.
1b. When you click on the alien, make him spawn in a new random spot and moving in a random direction 
1c. Add more aliens
1d. Add a score

2a. Make alien be controlled by the keyboard arrows
2b. Add a prize that spanws randomly. Make the alien move to get it. When it touches it, increase a score and then appear in a new random spot
2c. Aliens a 2-player game. Have one alien be "it" and chase the other one. One player can control the sprite with arrow keys, the other one with WASD keys. 

3a. Change the flappy bird sprites and background

4. Make a side-scrolling space shooter. 
4a. Make a sprite that is a spaceship that can move up and down
4b. Make the spaceship be able to shoot a laser
4c. Add an alien ship that moves up and down on the right
4d. Check for collisions between the laser and the alien ship
4e. Update and display a score each time it hits

Other ideas: 
#space invaders
https://magpi.raspberrypi.org/articles/pygame-zero-invaders
#Lots of other examples in 
https://github.com/lordmauve/pgzero/examples
